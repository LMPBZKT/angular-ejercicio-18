import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  Objetopadre1 = {
    CamisaDeColor: "Verde",
    Edad: "40 años",
    zapatos: "Cafes"
  };

  Objetopadre2 = {
    Vehiculo: 'Dodge 400',
    Color: 'Negro',
    Modelo: '2022'
  }

  constructor() { }

  ngOnInit(): void {
  }
}
